![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m122_picto.png)

[TOC]

# M122 - Grundlagen der Scriptsprache BASH (Linux, Unix)

---

### Lernprozess:
* **Themen** nacheinander durcharbeiten
* Die hier beschriebene Einführung **studieren** und sofort in der **Linux Command Line** (CL) ausprobieren
* Den **Checkpoint** durcharbeiten --> in der eigenen Linux Maschine!


* Für **Profis**, bzw. als **Nachschlagewerk** für die **Projektarbeit** sei auf das [**Openbook "Shell Programmierung"**](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_006_000.htm#Xxx999276) verwiesen: 

![Book](./x_gitressourcen/OpenBook.png)

Englische Anleitung:
[Bash Notes for Professionals](./BashNotesForProfessionals.pdf)

Hier noch ein kleiner Spick: [Command Line Cheat Sheet](https://cheatography.com/davechild/cheat-sheets/linux-command-line/)

---
<br><br><br>
---


# Teil 1: Linuxbefehle

-   Linux bietet etliche Befehle, welche in der Shell direkt oder in
    einem Skript gebraucht werden können wie zB. `cp`, `alias`, `cat`, `rm`.

-   Daneben bietet die Shell Programmierstrukturen wie zB. Schleifen,
    Verzweigungen, Variablen, usw.

-   Beide Elemente können in einem Skript verwendet werden, um Aufgaben
    auf dem System zu automatisieren

## Command Line Tool (Terminal) und der Linux-Prompt

-    Das CL-Tool (Terminal) ermöglicht es die Befehle der Bash abzusetzten. Dabei gibt es einige [Short-Cuts](https://www.howtogeek.com/howto/ubuntu/keyboard-shortcuts-for-bash-command-shell-for-ubuntu-debian-suse-redhat-linux-etc/).


-   Der Standard-Prompt (Eingabeaufforderung ) bildet sich nach dem Schema und kann geändert werden (`$PS1`) <br> 
    `<akt.username>@<hostname>: <akt.verzeichnis> #` oder <br> `<akt.username>@<hostname>:<akt.verzeichnis> $`

-    Die Eingabeaufforderung unter Linux (Prompt) sieht also z.B. wie folgt aus:
 
			user@host:/ #
			[user@host: ~ ]$
			user@host>
			user / %


-   Das Tildezeichen `~` ist ein Kürzel für das Heimatverzeichnis (Home) des aktuellen Benutzers

-   Die Heimatverzeichnisse befinden sich i.d.R. unter `/home/<benutzername>`.  <br> 
    Einzig Ausnahme: Das Heimatverzeichnis des Administratorusers
    (`root`) befindet sich unter `/root/`
    
-   Das Semikolon `;` trennt einzelne Befehle in einer Zeile voneinander (Aufreihung der Befehle: Sequenz)

-   Das Pipe-Zeichen  (AltGr-7) `|` verkettet einzelne Befehle in der Zeile (Piping)

-   Ein Hashtag `#` leitet einen Kommentar ein. 

-   Mit einem `\` am Ende einer Zeile lässt sich eine Befehlszeile auf der nächsten Zeile weiter fahren.
-   Das  `\` ist auch das sog. ESC-Zeichen, welches dem nachfolgenden Zeichen seine spezielle Bedeutung enthebt oder eine gibt: `\;`, `'\t'` oder `\\`. 

-   Mit einem `&` am Ende eines Befehls führt den Befehl im Hintergrund aus. Konsole, bzw. Script fährt weiter ohne zu warten.
    
## Hilfe holen

-   Der Befehl `man` öffnet sie Hilfeseiten (manual) eines Befehls <br> Syntax: `man <Befehlsname>`
    ![Tastenkürzel](x_gitressourcen/man Tastenkürzel.png)

-   Der Befehl `apropos` durchsucht alle Hilfeseiten nach einemn Stichwort <br> Syntax: `apropos <Stichwort>`

-   Der Befehl `which` findet den Ort eines installierten Programmes <br> Syntax: `which <Befehl>`



## Systemspezifische Befehle

-   Der Befehl `reboot` (oder `shutdown -r` oder `init 6`) startet das System neu

-   Der Befehl `halt` (`shutdown -h`, `init 0` oder `poweroff`) schaltet das System ab. <br> *(Raspi: 2 Minuten warten bis der Poweradapter abgezogen wird!)*

## Verzeichnisrelevante Befehle

-   Der Befehl `pwd` (*print working directory*) zeigt das aktuelle
    Verzeichnis an

-   Der Befehl `cd` ändert das aktuelle Verzeichnis. <br> Syntax: `cd <Zielverzeichnis>`

-   Der Befehl `mkdir` erstellt ein neues Verzeichnis. <br> Syntax: `mkdir <Verzeichnisname>`

-   Der Befehl `rmdir` löscht ein bestehendes Verzeichnis. <br> Syntax: `rmdir <Verzeichnisname>` (Verzeichnis muss leer sein!)

-   Der Befehl `ls` listet den Verzeichnisinhalt auf <br> Syntax: `ls <Verzeichnisname>`

-   Der Befehl `find` sucht Dateien innerhalb eines Verzeichnisses inkl. Unterverzeichnis. <br> Man kann auch direkt Befehle auf die gefundenen Objekte anwenden! <br> Syntax: `find [Verz] -type [f/d| -iname "Name" [command]`
    
[Checkpoint Übung 1](../03_Bash_Ueb/README.md)


## Dateirelevante Befehle

-   Der Befehl `cp` kopiert Dateien/Verzeichnisse <br> Syntax: `cp <Quelldatei> <Zieldatei>` <br> Syntax Verzeichnisse: `cp -R <Quellverzeichnis> <Zielverzeichnis>`

-   Der Befehl `rm` löscht Dateien/Verzeichnisse <br> Syntax: `rm <Zieldatei>` <br> Syntax Verzeichnisse: `rm -r <Zielverzeichnis>` (Vorsicht: `rm -r /` löscht innert Kürze das ganze System)

-   Der Befehl `mv` (move) verschiebt (= umbenennen) Dateien/Verzeichnisse <br> Syntax: `mv <altedatei> <neuedatei>`

-   Der Befehl `touch` erstellt eine neue leere Datei <br> Syntax: `touch <Dateiname>`

-   Der Befehl `cat` (catalogue) gibt Dateinhalt aus <br> Syntax: `cat <Zieldatei>`

-   Der Befehl `wc` (word count) zählt Wörter oder Linien eines Datei-Inhaltes <br> Syntax (linien): `wc -l <Zieldatei>` (-w zählt \# Wörter)

-   Der Befehl `echo` gibt eine Zeichenkette aus <br> Syntax: `echo "<Zeichenkette>"`


## Aliase (=Kurzbefehl)

-   Aliase dienen als nutzerspezifisches Kürzel für einen Befehl oder eine Befehlskombination

-   Ein Alias wird wie folgt gesetzt: `alias <aliasname>="<befehl mit params/args>"`

-   Beispiel:

        [user@host: /etc ]$ alias gohome="cd ~"
        [user@host: /etc ]$ pwd
         /etc 
        [user@host: /etc ]$ gohome
        [user@host: ~ ]$ pwd
         /home/user
        [user@host: ~ ]$ alias ll='ls -alFG'
        [user@host: ~ ]$ ll
         ...
        
        	
                
## Wildcards und Brace extension

-   \* steht für beliebig viele Zeichen

        ls *.txt

-   ? steht für ein beliebiges Zeichen

        ls file?.txt

-   `{ , }` erzeugt `File1.txt`, `File2.txt` und `File3.txt`:

        touch File{1,2,3}.txt

-   `{ .. }` erzeugt einen Bereich

        touch file{1..9}.txt
        ls file{1..9}.txt

-   `!` negiert einen Ausdruck

        ls file{!3}.txt


-   Auch Verschachtelungen sind möglich:

        touch file{orginal{.bak,.txt},kopie{.bak,.txt}}

    erzeugt `fileoriginal.txt`, `fileoriginal.bak`, `filekopie.txt` und
    `filekopie.bak`

[Checkpoint Übung 2](../03_Bash_Ueb/README.md)



## Tilde expansion


Einige nützliche Erweiterungen der Tilde:

    Heimverzeichnis des akt. Benutzers: ~ 

    Heimverzeichnis Benutzer: ~BENUTZERNAME     

    zuvor besuchtes Verzeichnis: ~- 

    akt. Arbeitsverzeichnis (pwd) : ~+ 

[Checkpoint Übung 3](../03_Bash_Ueb/README.md)

___
<br><br><br><br><br>
---


# Teil 2: Shellprogrammierung

Bevor wir unser erstes Script selber schreiben wollen wir mal einen Blick auf ein typische Script werfen: [install_bsp.sh](./install_bsp.md).

Dieses Script installiert automatisch, je nach gegebener Hardware, Treiber und lädt Software (avrdude) nach zu Bereitstellung einer Hardware Erweiterung. Einstellungen (u.a. Rechte) werden gemacht und Backups von Einstellungen werden getätigt ...

**Checkpoint**: 
Haben Sie die Variablen, die Kontrollstrukturen, die Kommentare und die Funktionen entdeckt? 

<br>

## Mein erstes Script

Ein Bash Script erstellen Sie unter Linux so:


1) Eine leere Datei mit Endung .sh erstellen.
Das können Sie in der Konsole mit folgendem Befehl machen:

```console
touch meinscript.sh
```

2) Diese Datei mit einem Texteditor bearbeiten. Wir wollen hier mal mit dem Editor 'Nano' einsteigen: 

```console
nano meinscript.sh
```

3) Dort können Sie mit `#!/bin/bash` (sog. [Shebang](https://linuxize.com/post/bash-shebang/): `#!/path/interpreter [arguments]` ) festlegen, dass der Script mit Bash ausgeführt werden soll und den Script mit Inhalten füllen. 
(Anmerkung: `#!/usr/bin/env bash`benutzt das `env`-Kommando um den BASH-Interpreter über die Pfad-Variable aufzurufen))

Beispiel:

```console
#!/bin/bash

echo “Das ist mein erstes Script“
```

Den Nano Editor schließen Sie mit der Tastenkombination Strg + X

Dann mit J (oder Y) das Abspeichern bestätigen und Enter drücken.


4) Script ausführbar machen: (Evtl. `sudo` vorne einfügen!)

```console
[sudo] chmod +x meinscript.sh
```

5) Nun können Sie den Script ausführen:

```console
./meinscript.sh
```

In der Konsole erscheint folgendes:

```console
“Das ist mein erstes Script“
```

Wenn das nicht funktioniert, sollten Sie das Resultat aus folgendem Befehl anschauen, was Ihnen sagt, wo der bash-Interpreter auf Ihrem System liegt:

```console
>  which bash
oder
> $SHELL
oder 
> BASH --version
```

Bemerkung:
- Um sich diese Zeichenkombination besser zu merken, denken Sie an die Wörter 'Häsch Bäng' (für '#!').


**Checkpoint Tutorial**
- Alternative Anleitung unter [https://www.learnshell.org/en/Hello%2C_World%21](https://www.learnshell.org/en/Hello%2C_World%21)
- Alternative Anleitung unter [https://linuxconfig.org/bash-scripting-tutorial](https://linuxconfig.org/bash-scripting-tutorial)

---

## Wahl eines Text-Editors
Empfohlene Editoren neben **nano**: , [Geany](https://www.geany.org/), [Texteditor GEdit](https://wiki.gnome.org/Apps/Gedit), [Atom](https://atom.io/). Diese Editoren sind komfortabel, müssen aber evtl. zusätzlich installiert werden.

> Für Profis: VS Code installieren auf [Debian/Ubuntu](https://ubunlog.com/de/visual-studio-code-editor-codigo-abierto-ubuntu-20-04/), [Raspberry Pi OS](https://code.visualstudio.com/docs/setup/raspberry-pi) (Evtl. direkt über Software-Install-App. Ca. 500MB !!)


---

# Variablen

Variablen sind **Behälter** mit Namen und einem gespeicherten Wert. Der Wert einer Variable wird immer als **Zeichenkette** abgespeichert, d.h. auch die (Integer-) Zahlen. 

Der Variablenname kann aus Groß- und Kleinbuchstaben, Zahlen und Unterstrichen bestehen, **darf allerdings niemals mit einer Zahl beginnen**.

Wichtig ist es auch, die Lebensdauer einer Variablen zu kennen, welche nur so lange wie die Laufzeit des Scripts (genauer der ausführenden Shell) währt. Beendet sich das Script, wird auch die Variable ungültig (sofern diese nicht exportiert wurde). 

-   Variablen werden mit dem Zuweisungsoperator `=` gesetzt. (Ohne Leerzeichen!)
-   Auf den Inhalt von Variablen kann mit einem vorangestellten `$` zugegriffen werden.
-   Der Inhalt einer Variable kann geändert werden
-   Nicht initialisierte Variablen haben eine leere Zeichenkette als Inhalt.
-   Der Variablenname ist case-sensitiv `$VAR != $var`
-   Eine Variable kann gelöscht werden: `VARNAME=`

		[user@host: ~ ]$ name="Hans"
		[user@host: ~ ]$ echo $name
		 Hans
		[user@host: ~ ]$ name="Muster"
		[user@host: ~ ]$ echo $name
		 Muster
		[user@host: ~ ]$ readonly var="fester_Wert"
		[user@host: ~ ]$ var="aendern?"
		 bash: var: Schreibgeschützte Variable.


-   Die Ausgabe eines Befehls kann einer Variable zugewiesen werden
-   Ein zu auszuwertender Ausdruck muss in `$( )` gesetzt werden
-   Der Inhalt von Variablen kann in anderen Befehlen weiterverwendet werden
-   Variablen können kopiert werden
-   Eine Variable kann mit dem `readonly`-Befehl als Konstante gesetzt werden

		[user@host: ~ ]$ datum=$(date +%Y_%m_%d)
		[user@host: ~ ]$ echo $datum
		 2022_10_06

		[user@host: ~ ]$ touch file_$datum
		[user@host: ~ ]$ ls
		 file_2022_10_06
		[user@host: ~ ]$ datum2=$datum; echo $datum2
		 2022_10_06
		
		
 
[Checkpoint Tutorial](https://www.learnshell.org/de/Variables) (Hinweis: Samstag -> Saturday)

Folgend die wichtigsten, automatisch verwalteten Variablen der Bash:

- `$LOGNAME` 	Login-Name des Benutzers
- `$0` Der Name des aufgerufenen Shellscripts
- `$1` - `$9`, `${10}`, ... ,  `$*` Parameter des aufgerufenen Shellscripts
- `$#` Anzahl Parameter des aufgerufenen Shellscripts
- `$$` Die Prozessnummer des aufgerufenen Shellscripts
- `$?` Der Beendigungsstatus eines Shellscripts
- `$!` Die Prozessnummer des zuletzt gestarteten Hintergrundprozesses
- `$PWD` 	Aktuelles Arbeitsverzeichnis
- `$OLDPWD` 	Der Wert ist das zuvor besuchte Arbeitsverzeichnis; wird vom Kommando cd gesetzt.
- `$HOME` 	Heimverzeichnis für den Benutzer; Standardwert für cd
- `$UID` 	Die User-ID des Anwenders. Diese Kennung ist in der Datei /etc/passwd dem Benutzernamen zugeordnet. 
- `$PATH` 	Suchpfad für die Kommandos (Programme); meistens handelt es sich um eine durch Doppelpunkte getrennte Liste von Verzeichnissen, in denen nach einem Kommando gesucht wird, das ohne Pfadangabe aufgerufen wurde; Standardwerte: PATH=:/bin:/usr/bin
- `$CDPATH` 	Suchpfad für das cd-Kommando
- `$SHELL`   Zeigt die aktuelle Shell mit dem Pfad an 
- `$RANDOM` 	Pseudo-Zufallszahl zwischen 0 bis 32767
- `$REPLY` 	Bei Menüs (select) enthält REPLY die ausgewählte Nummer.
- `$SECONDS` 	Enthält die Anzahl von Sekunden, die seit dem Start (Login) der aktuellen Shell vergangen ist.
- `$PROMPT_COMMAND` 	Hier kann ein Kommando angegeben werden, das vor jeder Eingabeaufforderung automatisch ausgeführt wird.
- `$PS1` 	Primärer Prompt; Prompt zur Eingabe von Befehlen.
- `$TZ` 	Legt die Zeitzone fest (hierzulande MET = Middle European Time) 

---

# Arithmetische Operatoren (Integer)

**Nur simple arithmetische Ganzzahl-Berechnungen** können mit der Shell berechnet werden. (Fliesskommazahlen nur mit dem Pipe-Tool `bc`.)

Es gibt in der Bash drei Varianten, um einen arithmetischen Ausdruck zu berechnen:

- `var=$(( Int-Arithmetik ))` 
- `var=$[ Int-Arithmetik ]`  

Die wichtigesten Operatoren sind:

| Opreation | Erklärung |
|:-----:|:----- |
| a + b | Addition (a plus b) |
| a - b | Substraction (a minus b) |
| a * b | Multiplication (a mal b) |
| a / b | Division (Ganzzahl) (a geteilt durch b) |
| a % b | modulo (der Ganzzahlrest von a geteilt durch b) |
| a ** b | Potenz (a hoch b) |

```console
		[user@host: ~ ]$ diff=$(( 100/5 ))
		[user@host: ~ ]$ echo $diff
		 20
```

[Checkpoint Tutorial](https://www.learnshell.org/de/Basic_Operators)

---
 
# Zeichenketten Verarbeitung (Strings)
 
Die Bash bietet Ihnen zur Verarbeitung von Zeichenketten einige eingebaute Funktionen an:

[Checkpoint Tutorial](https://www.learnshell.org/de/Basic_String_Operations) (Beachten Sie den Befehl **expr**! --> man expr)

[Tipps zu Text Processing Tools](https://blog.knoldus.com/play-with-text-in-linux-grep-cut-awk-sed/)

[Checkpoint Übung 4](../03_Bash_Ueb/README.md)

*Für das Projekt:*

Zum Bearbeiten von Zeichenketten werden immer noch vorrangig die alten UNIX-Tools (sofern auf dem System vorhanden) wie `tr`, `cut`, `paste`, `sed` und natürlich `awk` verwendet. Sofern Ihr Script überall laufen soll, sind Sie mit diesen Mitteln immer auf der sicheren Seite. 
	

[Checkpoint Übung 5](../03_Bash_Ueb/README.md)

---

# Arrays

![Selektion](./x_gitressourcen/array.png)

[Checkpoint Tutorial](https://www.learnshell.org/de/Arrays)

[Für Profis: Tutorial](https://linuxhint.com/bash-arrays-tutorial/) 



---

# Kontrollstrukturen
Um aus der »Shell« eine »echte« Programmiersprache zu machen, sind sogenannte Kontrollstrukturen erforderlich. Dabei handelt es sich um Entscheidungsverzweigungen oder Schleifen. Ebenso gehören Abstraktionen (Funktionen) dazu.

[Repetition m319: Aktivitätsdiagramm der Strukturen](./x_gitressourcen/Strukturen AD.png)


## Funktionen

![Abstraktion](./x_gitressourcen/Abstraktion.png)

[Checkpoint Tutorial](https://www.learnshell.org/de/Shell_Functions)


## Parameterübergabe (Args) beim Aufruf eines Scripts

![Args](./x_gitressourcen/Args.jpg)


[Checkpoint Tutorial](https://www.learnshell.org/de/Passing_Arguments_to_the_Script)

## Entscheidungen (If, Case)

![Selektion](./x_gitressourcen/Selektion.png)

[Checkpoint Tutorial **If mit Vergleichsoperatoren (Test)**](https://www.learnshell.org/de/Decision_Making)

[Checkpoint **If mit Befehlen**](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_006_000.htm#t2t31)

[Checkpoint **If mit Dateistatus**](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_006_004.htm#RxxKap00600404004E261F04B172)

[CheatSheet](https://kapeli.com/cheat_sheets/Bash_Test_Operators.docset/Contents/Resources/Documents/index)

> **Hinweis:** <br>
> Eine sehr interessante Besonderheit besteht darin, dass man die logischen Operatoren nicht nur für Ausdrücke verwenden kann, sondern auch zur logischen Verknüpfung von Anweisungen. So wird zum Beispiel bei einer Verknüpfung zweier Anweisungen mit dem ODER-Operator `||` die zweite Anweisung nur dann ausgeführt, wenn die erste fehlschlägt. Ein einfaches Beispiel in der Kommandozeile:
`>  rm file.txt 2>/dev/null ||  echo "file.txt konnte nicht gelöscht werden"`
Kann im Beispiel die Anweisung rm nicht ausgeführt werden, wird die Anweisung hinter dem ODER-Operator ausgeführt. Ansonsten wird eben nur rm zum Löschen der Datei file.txt ausgeführt.

## Schleifen (while, until, for)

![Iteration](./x_gitressourcen/Iteration.png)

[Checkpoint Tutorial](https://www.learnshell.org/de/Loops)

### Tricks mit Beispielen:

1) Wenn Sie sich die for-Schleife mit der Parameterliste ansehen, dürften Sie wohl gleich auf die Auswertung der Argumente in der Kommandozeile kommen. for scheint wie geschaffen, diese Argumente zu verarbeiten. Hierzu müssen Sie für die Parameterliste lediglich die Variable $@ verwenden. Das folgende Script überprüft alle Dateien, die Sie in der Kommandozeile mit angeben auf Ihren Typ.

		#!/bin/bash
		# Demonstriert die Verwendung von for mit Argumenten
		# afor1.sh
		for datei in "$@"
		do
		   [ -f $datei ] && echo "$datei: Reguläre Datei"
		   [ -d $datei ] && echo "$datei: Verzeichnis"
		   [ -b $datei ] && echo "$datei: Gerätedatei(block special)"
		   [ -c $datei ] && echo "$datei: Gerätedatei(character special)"
		   [ -t $datei ] && echo "$datei: serielles Terminal"
		   [ ! -e $datei ] && echo "$datei: existiert nicht"
		done


Ausführung:
`./afor1.sh Documents file.txt /dev/tty /dev/cdrom gibtsnicht.dat`

2) Wollen Sie z. B. alle Dateien mit der Endung ».txt« ausgeben, können Sie folgendermaßen vorgehen:

		#!/bin/bash
		# Demonstriert die Verwendung von for und der Datei-Substitution
		# afor3.sh
		# Gibt alle Textdateien des aktuellen Arbeitsverzeichnisses aus
		for datei in *.txt
		do
		   echo $datei
		   [ -r $datei ] && echo "... ist lesbar"
		   [ -w $datei ] && echo "... ist schreibbar"
		done

3) For kann auch das Resultat von Befehlen verwerten. Es sei bspw. folgende (versteckte) Datei namens .userlist (ohne Extension) mit einer Auflistung aller User, die bisher auf dem System registriert sind, gegeben:

		[user@host: ~ ]$ cat .userlist
		 tot
		 you
		 rot
		 john
		 root
		 martin

Wollen Sie jetzt an alle User, die hier aufgelistet und natürlich im Augenblick eingeloggt sind, eine Nachricht senden, gehen Sie wie folgt vor:

		#!/bin/bash
		# Demonstriert die Verwendung von for und
		# der Kommando-Substitution
		# afor6.sh
		# Komplette Argumentenliste für News an andere User verwenden
		NEU="$*"
		for user in $(cat .userlist)    # Or use Backtick!
		do
		   if who | grep ^$user > /dev/null
		   then
		      echo $NEU | write $user
		      echo "Verschickt an $user"
		   fi
		done


4) Selbstverständlich eignet sich die for-Schleife hervorragend für die Arrays. Die Parameterliste lässt sich relativ einfach mit `${array[*]}` realisieren.

		#!/bin/bash
		# Demonstriert die Verwendung von for und der Arrays
		# afor7.sh
		# Liste von Werten in einem Array speichern
		# Version: Korn-Shell (auskommentiert)
		#set -A array 1 2 3 4 5 6 7 8 9
		# Version: bash
		array=( 1 2 3 4 5 6 7 8 9 )
		# Alle Elemente im Array durchlaufen
		for value in ${array[*]}
		do
		   echo $value
		done

Das Script bei der Ausführung:

		[user@host: ~ ]$ ./afor7.sh
		 1
		 2
		 3
		 4
		 5
		 6
		 7
		 8
		 9

## Zusammenfassung

Hier finden wir für "alle Fälle" was:
> [Cheat Sheet Kontrollstrukturen](https://learntutorials.net/de/bash/topic/420/kontrollstrukturen)




---


# Informationskanäle

![in out err](./x_gitressourcen/inouterr.png)

Wenn sie mit der Shell arbeiten, gibt es unterschiedliche
Informationskanäle, welche sie verwenden können.

-   `stdin` - Standardeingabekanal (0) <br>
    (z.B. sie geben Zeichen über die Tastatur ein)

-   `stdout` - Standardausgabekanal (1) <br>
    (z.B. ein Programm zeigt den Inhalt eines Verzeichnisses am
    Bildschirm an)

-   `sterr` - Standardfehlerausgabekanal (2) <br>
    (z.B. ein Programm erzeugt einen Fehler und zeigt diesen am
    Bildschirm an)

Jeder der Kanäle kann über die jeweilige Nummer angesprochen werden
(0,1,2)


## Ausgabe umleiten

![in out err](./x_gitressourcen/ausuml.png)


Die Ausgabe (stdout) eines Befehls kann umgeleitet werden mit `>` oder `>>` *<br>*
Beispiele:

-   `ls -la > liste.txt`

-   `./meinskript > outputofscript.txt`

-   `cat outputofscript.txt >> list.txt`

`>>` hängt Inhalt an bestehende Datei an, `>` überschreibt den Inhalt
komplett mit Neuem


Die unterschiedlichen Kanäle können mit der Nummer spezifiziert werden:

![in out err](./x_gitressourcen/uml2.png)

-   `./meinskript 2> errorsofscript.txt` 
    (Leitet nur Fehlermeldungen in die Datei `errorsofscript.txt`)

-   `./meinskript 1> outputofscript.txt`<br>
    (Leitet den üblichen Output in die Datei `outputofscript.txt`)

-   `./meinzweitesskript 2>> errorsofscript.txt`<br>
    (Dasselbe geht auch im Anhängemodus)

-   `./skript 1> output.txt 2> error.txt`<br>
    (Unterschiedliche Umleitungen der Kanäle in einem Befehl)
    
Es gibt einen "Abfall Eimer": `> /dev/null`. Darin "verschwinden" alle Ausgaben.


## Ausgabekanäle zusammenlegen / Ausgaben unterdrücken

Will man Standardausgabe und Standardfehlerausgabe über denselben Kanal ausgeben, kann man diese mit `2>&1` (Leitet `stderr` in `stdout`) koppeln:

`./skript > output.txt 2>&1` 

Hier ist die Reihenfolge von `> output.txt` und ` 2>&1` wichtig. Umgekehrt funktioniert es nicht wie erwünscht.

Will man einen Ausgabekanal *ausschalten*, kann dieser nach `/dev/null` (der Linux-Datenschredder) umgeleitet werden:

`./skript > output.txt 2>/dev/null`

(Unterdrückt die Ausgabe von Fehlern)


## Eingabe umleiten

![in out err](./x_gitressourcen/einuml.png)


Gleichwohl kann die Standardeingabe (oder Ein- und Ausgabe gleichzeitig) umgeleitet werden

-   `cat < meinFile.txt`

-   `cat < meinFile.txt > meinKopiertesFile.txt`

-   Manuelle Eingabe: `<<` fängt eine interaktive Eingabe ab, bis ein Schlüsselwort zu Terminierung eingegeben wird (zB. `fertig`). Anwendbungsfall: Um inteaktive Tools

```console
    [user@host: ~ ]$ sort << fertig 
    [user@host: ~ ]$ Z
    [user@host: ~ ]$ B
    [user@host: ~ ]$ A
    [user@host: ~ ]$ fertig
        A
        B
        Z
```
> **Praktischer Anwendbungsfall**: Um inteaktive Tools (wie ftp oder ssh) mit programmierten Eingaben zu füttern!


## Pipeline

Das Konzept der Pipeline ist sehr effektiv beim Shellscripting! (Hint: Im Gegensatz zu Nordstream sollte es oft eingesetzt werden!!!) 

![in out err](./x_gitressourcen/pipeline.png)

In einer Pipeline wird die Ausgabe (stdout) des vorhergehenden
Befehls als textueller Output an den nächsten weitergereicht:

*(Hinweis: Im Gegensatz zu Powershell ist die Pipeline in der Linuxshell nicht
objektorientiert sondern rein textbasiert!)*

-   Filtert alle Zeilen mit dem Begriff `hallo` aus der Datei
    `meinFile.txt`:

        cat meinFile.txt | grep hallo

-   Filtert und sortiert alle Zeilen mit dem Begriff `hallo` aus der
    Datei `meinFile.txt` (ohne Duplikate):

        cat meinFile.txt | grep hallo | uniq | sort

-   liefert eine Liste aller Benutzernamen (Alles vor dem ersten
    Doppelpunkt in jeder Zeile in `/etc/passwd`), ausser dem Benutzer
    irc.

        cat /etc/passwd | grep -v irc | cut -d ’:’ -f 1
        
[Checkpoint Übung 6](../03_Bash_Ueb/README.md)

---

# Ein Script debuggen

[Fehlerarten](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_012_001.htm#RxxKap01200104004F791F019172)

[Fehlersuche](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_012_002.htm#RxxKap01200204004F7A1F023172)

[Checkpoint Tutorial](https://www.shell-tips.com/bash/debug-script/)
(Rechnen Sie dazu genügend Zeit ein!)



---
<br><br><br><br><br>
---

# Teil 3: Rechte und Benutzerverwaltung

## Zugriffsrechte

Die Rechte sind ein grösseres Kapitel von Linux. 

- Es werden drei "Benutzerkreise" für Dateien / Verzeichnisse unterschieden: **Eigentümer** (Owner), **Gruppe** (group) und die **Anderen** (others).

- Das erste bestimmt den Typen des Verzeichniseintrages. Es gibt normale Dateien (`-`), Verzeichnisse (`d`), Devices files (`b` und `c`), und noch ein paar Typen mehr.

- Die Zeichen (Bits) für die Zugriffe sind: `r` - Read, `w` - Write, `x` - eXecute.

- Drei Zeichengruppen zu je drei Zeichen kennzeichnen die Zugriffsrechte für die Datei bzw. das Verzeichnis. Hat der Benutzer/Gruppe/andere ein Recht, so wird der Buchstabe dafür angezeigt; ansonsten wird ein - dafür angezeigt.

So ist eine Auflistung mit `ls -al` dargestellt:

![Rechte mit ls](./x_gitressourcen/Rechte_ls.jpg)

*Erklärung zu den Anzeigespalten:*

1.	Rechte für die entsprechende Datei oder das Verzeichnis
2.	Anzahl Referenzen auf das Verzeichnis oder die Datei. Normalerweise 1 für Datei, 2+ für Anzahl Verzeichnis-Einträge im Verzeichnis (inkl. . und ..)
3.	Benutzer, der die Datei besitzt (erstellt hat)
4.	Benutzergruppe, die Zugriff auf die Datei hat
5.	Dateigrösse in Bytes
6.	Letzte Änderungszeit
7.	Name der Datei oder des Verzeichnisses  (. = aktuelles Verzeichnis, .. = Übergeordnetes Verzeichnis)


Verschaffen Sie sich hier einen weiteren Überblick:
[Rechte](https://wiki.ubuntuusers.de/Rechte/)

-   Der Befehl `chown` wechselt den Eigentümer einer Datei oder eines Verzichnisses.

-   Der Befehl `chmod` wechselt die Zugriffsrechte einer Datei oder eines Verzichnisses.


## Befehle für Benutzer und Gruppen

Verschaffen Sie sich hier einen Überblick über Benutzer und Gruppen:
[Benutzer und Gruppen](https://wiki.ubuntuusers.de/Benutzer_und_Gruppen/)

-   Der Befehl `whoami` zeigt den aktuellen Benutzernamen an

-   Der Befehl `who` zeigt alle am System angemeldeten Benutzer an

-   Der Befehl `groups` zeigt die Gruppen des aktuellen Benutzernamen an

-   Der Befehl `id` zeigt die Nutzerid und Gruppen des aktuellen
    Benutzers an

-   Der Befehl `su` wechselt den aktuellen Benutzer <br>
    Syntax: `su - <User>` <br>
    ( `-` sorgt dafür, dass wie beim Login alle Login-Skripte (.bashrc, .profile,...) durchlaufen werden und dass ins Heimverzeichnis des neuen Users gewechselt wird. Ohne - wird nur die User-Id gewechselt.)

## Userspezifische Befehle

-   Der Befehl `useradd` fügt einen neuen Benutzer hinzu <br>
    Syntax: `useradd <User>`

-   Der Befehl `userdel` löscht einen bestehenden Benutzer <br>
    Syntax: `userdel <User>`

-   Der Befehl `passwd` kann (unter anderem) das Passwort wechseln <br>
    Syntax: `passwd <User>`

-   Der Befehl `logout` loggt den aktuellen Benutzer vom System aus
    (ebenso `exit`)



---
<br><br><br><br><br>
---

# Anhang

Die meisten CL-Tools sind zeilenorientiert! Suche und Ergebnisse beziehen sich also auf ganze Zeilen, bzw. deren Inhalt! 

## Reguläre Ausdrücke um spezifisch zu suchen

[REGEX](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_013_000.htm#RxxKap01300004004F851F035174)

## GREP-Tool um Zeichen in Zeilen zu finden

[GREP](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_013_001.htm#RxxKap01300104004F9C1F039172)

## SED-Tool um z.B. einzelne Zeichen in Zeilen zu läschen / ersetzen

[SED](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_014_000.htm#Xxx999276)

## AWK-Programmierung

[AWK](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_015_000.htm#Xxx999276)

## cURL Datensauger

[cURL](https://curl.se/)

## Tipps: Test ob Zeichen oder Zahlen

[Eingabe testen](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_017_000.htm#t2t32)


## "Alle" BASH Befehle und Tools im Überblick --> Projektarbeit
 
[Referenz](https://openbook.rheinwerk-verlag.de/shell_programmierung/shell_016_000.htm#Xxx999276)


