![TBZ Logo](../../x_gitressourcen/tbz_logo.png)
![m319 Picto](../../x_gitressourcen/m122_picto.png)

[TOC]

# M122-Server - Dokumentation

---

# Umsetzung folgender Ideen möglich (BASH / Linux):

Folgend Ideen zur **Erhebung von RAW-Daten** vom Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-API):

- SQL-Daten: Serverstatus, Berechtigungen, Tabellendaten bestimmter Datenbanken (.sql), ...
- Strukturierte Daten: Dateisystem, Berechtigungen, .cvs, .json, ...
- Status / Performance Daten: Systemdaten, Daten von Perfomance-Tools, Dienstdaten, ...
- IoT-Daten: **Sensordaten**

Folgend Ideen zur **Verarbeitung und Übermittlung von FMT-Daten** an Kundensystems / -dienstes (Linux Server, Raspberry Pi, Web-Dienst):

- Publish: Darstellen der Daten *WEB*
- DB Storage: Datenbank SQL insert, ...



