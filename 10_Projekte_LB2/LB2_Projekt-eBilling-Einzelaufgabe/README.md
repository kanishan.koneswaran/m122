
### FTP

- [https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/](https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/)
		
**bash-Beispiel**
<br>
![../tools-technics/ftpanweisungen.jpg](../tools-technics/ftpanweisungen.jpg)

**cmd-Beispiel**
<br>
![../tools-technics/cmd-ftp-start.bat.jpg](../tools-technics/cmd-ftp-start.bat.jpg)
![../tools-technics/cmdcommands.ftp.jpg](../tools-technics/cmdcommands.ftp.jpg)


### E-Mail

- [https://www.linuxfordevices.com/tutorials/linux/mail-command-in-linux](https://www.linuxfordevices.com/tutorials/linux/mail-command-in-linux)
- [https://www.geeksforgeeks.org/send-mails-using-a-bash-script](https://www.geeksforgeeks.org/send-mails-using-a-bash-script)
