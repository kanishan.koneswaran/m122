BEWERTUNGSKRITERIEN
(MUH, 2022-05)

M122-Übung "e-bill"
=====================

(2P)
- Filedownload (.data) via FTP (aus dem [Kundenserver]/out/XX21xMustermann)
	- [2] es können auch mehrere Dateien "gleichzeitig" verarbeitet werden
	
(6P)
- Lesen der Input-Datei (.data)
	- [2] Aufspalten der Informationen
	- [3] Erkennen falscher Informationen
	- [1] Rückweisen falscher/inkorrekter Rechnung

(11P)
- Erstellung der Invoice.txt
	- [1] Richtiger Filename (gem. definierter Vorgabe)
	- [4] Korrekte Darstellung und Formatierung der Rechnung mit Einrückung und Kollonierung der Rechnungzeilen
	- [3] Richtige Berechnung der End-Summe inkl. Darstellung (2-Nummen nach dem Dez-Punkt)
	- [2] Einrücken und Darstelllung des Einzahlungsschein-Abschnitts
	- [1] Richtige Berechnung des Zahlungsziel-Datum (Rechnungsdatum + Zahlungsziel), -> nicht Verarbeitungsdatum!!

(6P)
- Erstellung der Invoice.xml
	- [1] Richtiger Filename (gem. definierter Vorgabe)
	- [1] Rechnungsnummer eingesetzt
	- [2] Summe korrekt (ohne Punkt, führende Nullen)
	- [2] Zahlungsziel & Zahlungszieldatum (Korr. Datum und Formattierung)

(2P)
- Fileupload (2 Files (.txt und .xml) pro Rechnung)
	- [2] via FTP (nach [Zahlungsserver]/in/XX21xMustermann)

(3P)
- Zip- oder tar-File Erstellung
	- [2] Zip-/tar-File mit korrektem Inhalt und Dateinamen
	- [1] Fileupload via FTP (auf den [Kundenserver]/in/XX21xMustermann)

(5P)
- Mail
	- [2] Mail-Versand (kommt an der richtigen Adresse "heute/jetzt" an (Mailadr im Input))
	- [2] Mail-Text und Absender fehlerlos, den Anforderungen entsprechend
	- [1] Mail-Attachment (.zip/.tar) geschickt/vorhanden

(5P)
- Konfiguration und Projektdateiorganisation
	- [2] "gute" Struktur der Projektdateien, Verarbeitungsdaten nicht bei den Verarbeitungs-Skript(s)
	- [2] Log-File mit vernünftigen/aussagekräftigen Informationen, z.B. Erkennung von fehlerhafter Verarbeitung
	- [1] separate Konfigurationsdatei

(2P)
- Automatisierung
	- [2] Scheduler eingerichtet und funktioniert (Linux "crontab" oder Win "Aufgabenplaner")

(42P = Total)

NOTENTABELLE
max.	42.00	100%	6.00	6.0
	41.00	98%	5.88	5.9
	40.00	95%	5.76	5.8
	39.00	93%	5.64	5.6
	38.00	90%	5.52	5.5
	37.00	88%	5.40	5.4
	36.00	86%	5.29	5.3
	35.00	83%	5.17	5.2
	34.00	81%	5.05	5.0
	33.00	79%	4.93	4.9
	32.00	76%	4.81	4.8
	31.00	74%	4.69	4.7
	30.00	71%	4.57	4.6
	29.00	69%	4.45	4.5
	28.00	67%	4.33	4.3
	27.00	64%	4.21	4.2
	26.00	62%	4.10	4.1
	25.00	60%	3.98	4.0
	24.00	57%	3.86	3.9
	23.00	55%	3.74	3.7
	22.00	52%	3.62	3.6
	21.00	50%	3.50	3.5
	20.00	48%	3.38	3.4
	19.00	45%	3.26	3.3
	18.00	43%	3.14	3.1
	17.00	40%	3.02	3.0
	16.00	38%	2.90	2.9
	15.00	36%	2.79	2.8
	14.00	33%	2.67	2.7
	13.00	31%	2.55	2.5
	12.00	29%	2.43	2.4
	11.00	26%	2.31	2.3
	10.00	24%	2.19	2.2
	9.00	21%	2.07	2.1
	8.00	19%	1.95	2.0
	7.00	17%	1.83	1.8
	6.00	14%	1.71	1.7
	5.00	12%	1.60	1.6
	4.00	10%	1.48	1.5
	3.00	7%	1.36	1.4
	2.00	5%	1.24	1.2
	1.00	2%	1.12	1.1
	0.00	0%	1.00	1.0
 
	