# m122

## Tools & Technics

### SSH

Eine kurze Anleitung wie man [mit **ssh** ohne Passwort](ssh.md) auf einen Fremdrechner verbunden werden kann

### FTP

- [https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/](https://www.howtoforge.com/tutorial/how-to-use-ftp-on-the-linux-shell/)

		
**bash-Beispiel**
<br>
![./ftpanweisungen.jpg](./ftpanweisungen.jpg)

**cmd-Beispiel**
<br>
![./cmd-ftp-start.bat.jpg](./cmd-ftp-start.bat.jpg)
![./cmdcommands.ftp.jpg](./cmdcommands.ftp.jpg)


### E-Mail

- [https://www.linuxfordevices.com/tutorials/linux/mail-command-in-linux](https://www.linuxfordevices.com/tutorials/linux/mail-command-in-linux)
- [https://www.geeksforgeeks.org/send-mails-using-a-bash-script](https://www.geeksforgeeks.org/send-mails-using-a-bash-script)


**Mailing mit Python** (Dank an Hr. Noel L. Hug, AP21a)

import win32com.client as win32
<br>
![./mailing-mit-python.jpg](mailing-mit-python.jpg)



**Mailing mit PowerShell**

[./mailing-mit-powershell](./mailing-mit-powershell)
<br>
![./mailing-mit-powershell.jpg](./mailing-mit-powershell.jpg)




**Mailing mit PHP** (nur auf Webserver!)

Diese Art funktioniert nur auf einem Webserver, auf dem ein Mailserver installiert ist, was bei allen teuren und billigen Internetprovidern normal ist. Wenn es auf dem lokalen Rechner funltionieren soll, muss zuerst ein Mailserver lokal installiert werden.

<br>
![./mailing-mit-phpwebserver.jpg](./mailing-mit-phpwebserver.jpg)
